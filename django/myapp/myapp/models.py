
from django.db import models


class Ciudades(models.Model):
    idciudad = models.AutoField(primary_key=True) 
    ciudad = models.CharField(max_length=60) 


class CodigoDeArea(models.Model):
    idcodigo_de_area = models.AutoField(primary_key=True)  
    codigo_de_area = models.CharField(max_length=45) 

class CodigosPostales(models.Model):
    idcodigo_postal = models.AutoField(primary_key=True) 
    codigos_postal = models.CharField(max_length=45)


class Estados(models.Model):
    idestado = models.AutoField(primary_key=True)
    estado = models.CharField(max_length=60) 


class Municipios(models.Model):
    idmunicipio = models.AutoField(primary_key=True)  
    municipio = models.CharField(max_length=60) 


class Personas(models.Model):
    idpersona = models.AutoField(primary_key=True) 
    nombres = models.CharField(max_length=150)  
    apellidos = models.CharField(max_length=150)  
    cedula = models.IntegerField(max_length=10) 
    correo = models.EmailField(max_length=254)  
    fechanacimiento = models.DateField()  

