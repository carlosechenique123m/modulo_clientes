# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Ciudades(models.Model):
    idciudad = models.AutoField(db_column='idCiudad', primary_key=True)  # Field name made lowercase.
    ciudad = models.CharField(db_column='Ciudad', max_length=60)  # Field name made lowercase.


class CodigoDeArea(models.Model):
    idcodigo_de_area = models.AutoField(db_column='idCodigo de Area', primary_key=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    codigo_de_area = models.CharField(db_column='Codigo de Area', max_length=45)  # Field name made lowercase. Field renamed to remove unsuitable characters.



class CodigosPostales(models.Model):
    idcodigo_postal = models.AutoField(db_column='idCodigo Postal', primary_key=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    codigos_postal = models.CharField(db_column='Codigos Postal', max_length=45)  # Field name made lowercase. Field renamed to remove unsuitable characters.



class Estados(models.Model):
    idestado = models.AutoField(db_column='idEstado', primary_key=True)  # Field name made lowercase.
    estado = models.CharField(db_column='Estado', max_length=60)  # Field name made lowercase.


class Municipios(models.Model):
    idmunicipio = models.AutoField(db_column='idMunicipio', primary_key=True)  # Field name made lowercase.
    municipio = models.CharField(db_column='Municipio', max_length=60)  # Field name made lowercase.


class Personas(models.Model):
    idpersona = models.AutoField(db_column='idPersona', primary_key=True)  # Field name made lowercase.
    nombres = models.CharField(db_column='Nombres', max_length=150)  # Field name made lowercase.
    apellidos = models.CharField(db_column='Apellidos', max_length=150)  # Field name made lowercase.
    cedula = models.IntegerField(db_column='Cedula')  # Field name made lowercase.
    correo = models.CharField(db_column='Correo', max_length=150)  # Field name made lowercase.
    fechanacimiento = models.DateField(db_column='FechaNacimiento')  # Field name made lowercase.


class Rol(models.Model):
    idrol = models.AutoField(db_column='idRol', primary_key=True)  # Field name made lowercase.
    rol = models.CharField(db_column='Rol', max_length=45)  # Field name made lowercase.


class Telefonos(models.Model):
    idtelefono = models.AutoField(db_column='idTelefono', primary_key=True)  # Field name made lowercase.
    personas_idpersonas = models.ForeignKey('Personas', on_delete=models.CASCADE, db_column='Personas_idPersonas')  # Field name made lowercase.
    codigo_de_area_idcodigo_de_area = models.ForeignKey('CodigoDeArea', on_delete=models.CASCADE, db_column='Codigo de Area_idCodigo de Area')  # Field name made lowercase. Field renamed to remove unsuitable characters.
    telefono = models.IntegerField(db_column='Telefono')  # Field name made lowercase.



class UserKeys(models.Model):
    iduser_keys = models.AutoField(primary_key=True)
    user_keys = models.CharField(max_length=45)



class Direcciones(models.Model):
    iddirecciones = models.AutoField(db_column='idDirecciones', primary_key=True)  # Field name made lowercase.
    personas_idpersona = models.ForeignKey('Personas', on_delete=models.CASCADE, db_column='Personas_idPersona')  # Field name made lowercase.
    estados_idestado = models.ForeignKey('Estados', on_delete=models.CASCADE, db_column='Estados_idEstado')  # Field name made lowercase.
    ciudades_idciudad = models.ForeignKey('Ciudades', on_delete=models.CASCADE, db_column='Ciudades_idCiudad')  # Field name made lowercase.
    municipios_idmunicipio = models.ForeignKey('Municipios', on_delete=models.CASCADE, db_column='Municipios_idMunicipio', blank=True, null=True)  # Field name made lowercase.
    codigo_postal_idcodigo_postal = models.ForeignKey('CodigosPostales', on_delete=models.CASCADE, db_column='Codigo Postal_idCodigo Postal', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.




class RolHasPersonas(models.Model):
    rol_idrol = models.OneToOneField(Rol, on_delete=models.CASCADE, db_column='Rol_idRol', primary_key=True)  # Field name made lowercase.
    personas_idpersonas = models.ForeignKey('Personas', on_delete=models.CASCADE, db_column='Personas_idPersonas')  # Field name made lowercase.




class Usuarios(models.Model):
    idusuario = models.AutoField(db_column='idUsuario', primary_key=True)  # Field name made lowercase.
    user = models.CharField(max_length=45)
    password = models.CharField(db_column='Password', max_length=45)  # Field name made lowercase.
    personas_idpersonas = models.ForeignKey('Personas', on_delete=models.CASCADE, db_column='Personas_idPersonas')  # Field name made lowercase.
    user_keys_iduser_keys = models.ForeignKey('UserKeys', on_delete=models.CASCADE, db_column='user_keys_iduser_keys')

