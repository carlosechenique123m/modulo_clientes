from django.shortcuts import render
from django.http.response import JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from .models import Personas
import json

# Create your views here.
class PersonasView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, id=0):
        if (id>0):
            personas=list(Personas.objects.filter(idpersona=id).values())
            if len(personas) > 0:
                persona=personas[0]
                datos={'message':"Success",'personas':persona}
            else:
                datos={'message':"Personas not found..."}
            return JsonResponse(datos)
        else:
            personas= list(Personas.objects.values())
            if len(personas) > 0:
                datos={'message':"Success",'personas':personas}
            else:
                datos={'message':"Personas not found..."}
            return JsonResponse(datos)

    def post(self,request):
        # print(request.body)
        jd=json.loads(request.body)
        # print(jd)
        Personas.objects.create(nombres=jd['nombres'], apellidos=jd['apellidos'], cedula=jd['cedula'], correo=jd['correo'], fechanacimiento=jd['fechanacimiento'])
        datos={'message':"Success"}
        return JsonResponse(datos)

    def put(self,request,id):
        jd=json.loads(request.body)
        personas=list(Personas.objects.filter(id=id).values())
        if len(personas) > 0:
            persona=Personas.objects.get(id=id)
            persona.Nombres=jd['Nombres']
            persona.Apellidos=jd['Apellidos']
            persona.Cedula=jd['Cedula']
            persona.Correo=jd['Correo']
            persona.FechaNacimiento=jd['FechaNacimiento']
            persona.save()
            datos={'message':"Success"}
        else:
            datos = {'message': "Personas not found"}
        return JsonResponse(datos)

    def delete(self,request, id):
        personas=list(Personas.objects.filter(idpersona=id).values())
        if len(personas) > 0:
            Personas.objects.filter(idpersona=id).delete()
            datos={'message':"Success"}
        else:
            datos = {'message': "Personas not found"}
        return JsonResponse(datos)

