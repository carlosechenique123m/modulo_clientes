from django.urls import path
from .views import PersonasView

urlpatterns=[
    path('Personas/',PersonasView.as_view(), name='personas_list'),
    path('Personas/<int:id>',PersonasView.as_view(), name='personas_process')
]