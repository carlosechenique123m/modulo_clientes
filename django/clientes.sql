-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-12-2022 a las 04:58:53
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `clientes`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `api_ciudades`
--

CREATE TABLE `api_ciudades` (
  `idCiudad` int(11) NOT NULL,
  `Ciudad` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `api_ciudades`
--

INSERT INTO `api_ciudades` (`idCiudad`, `Ciudad`) VALUES
(1, 'Acarigua'),
(2, 'Anaco'),
(3, 'Araure'),
(4, 'Barcelona'),
(5, 'Barinas'),
(6, 'Barquisimeto'),
(7, 'Boconó'),
(8, 'Cabimas'),
(9, 'Cabudare'),
(10, 'Cagua'),
(11, 'Calabozo'),
(12, 'Caracas'),
(13, 'Carora'),
(14, 'Carúpano'),
(15, 'Charallave'),
(16, 'Ciudad Bolívar'),
(17, 'Ciudad Guayana'),
(18, 'Ciudad Ojeda'),
(19, 'Coro'),
(20, 'Cúa'),
(21, 'Cumaná'),
(22, 'El Limón'),
(23, 'El Tigre'),
(24, 'El Tocuyo'),
(25, 'El Valle del Espiritu Santo'),
(26, 'El Vigía'),
(27, 'Guacara'),
(28, 'Guanare'),
(29, 'Guarenas'),
(30, 'Guasdualito'),
(31, 'Guatire'),
(32, 'Güigüe'),
(33, 'La Asunción'),
(34, 'La Victoria'),
(35, 'Los Guayos'),
(36, 'Los Teques'),
(37, 'Machiques'),
(38, 'Maracaibo'),
(39, 'Maracay'),
(40, 'Mariara'),
(41, 'Maturín'),
(42, 'Mérida'),
(43, 'Ocumare del Tuy'),
(44, 'Palo Negro'),
(45, 'Porlamar'),
(46, 'Puerto Cabello'),
(47, 'Puerto La Cruz'),
(48, 'Puerto Píritu'),
(49, 'Punta de Mata'),
(50, 'Punto Fijo'),
(51, 'Quibor'),
(52, 'San Carlos'),
(53, 'San Cristóbal'),
(54, 'San Felipe'),
(55, 'San Fernando de Apure'),
(56, 'San Juan de los Morros'),
(57, 'Santa Lucía'),
(58, 'Santa Rita'),
(59, 'Socopó'),
(60, 'Táriba'),
(61, 'Tinaquillo'),
(62, 'Tocuyito'),
(63, 'Trujillo'),
(64, 'Turmero'),
(65, 'Valencia'),
(66, 'Valera'),
(67, 'Valle de la Pascua'),
(68, 'Villa de Cura'),
(69, 'Yaritagua');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `api_codigodearea`
--

CREATE TABLE `api_codigodearea` (
  `idCodigo de Area` int(11) NOT NULL,
  `Codigo de Area` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `api_codigospostales`
--

CREATE TABLE `api_codigospostales` (
  `idCodigo Postal` int(11) NOT NULL,
  `Codigos Postal` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `api_direcciones`
--

CREATE TABLE `api_direcciones` (
  `idDirecciones` int(11) NOT NULL,
  `Ciudades_idCiudad` int(11) NOT NULL,
  `Codigo Postal_idCodigo Postal` int(11) DEFAULT NULL,
  `Estados_idEstado` int(11) NOT NULL,
  `Municipios_idMunicipio` int(11) DEFAULT NULL,
  `Personas_idPersona` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `api_estados`
--

CREATE TABLE `api_estados` (
  `idEstado` int(11) NOT NULL,
  `Estado` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `api_estados`
--

INSERT INTO `api_estados` (`idEstado`, `Estado`) VALUES
(1, 'Amazonas'),
(2, 'Anzoátegui'),
(3, 'Apure'),
(4, 'Aragua'),
(5, 'Barinas'),
(6, 'Bolívar'),
(7, 'Carabobo'),
(8, 'Cojedes'),
(9, 'Delta Amacuro'),
(10, 'Dependencias Federales'),
(11, 'Distrito Federal'),
(12, 'Falcón'),
(13, 'Guárico'),
(14, 'Lara'),
(15, 'Mérida'),
(16, 'Miranda'),
(17, 'Monagas'),
(18, 'Nueva Esparta'),
(19, 'Portuguesa'),
(20, 'Sucre'),
(21, 'Táchira'),
(22, 'Trujillo'),
(23, 'Vargas'),
(24, 'Yaracuy'),
(25, 'Zulia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `api_municipios`
--

CREATE TABLE `api_municipios` (
  `idMunicipio` int(11) NOT NULL,
  `Municipio` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `api_personas`
--

CREATE TABLE `api_personas` (
  `idPersona` int(11) NOT NULL,
  `Nombres` varchar(150) NOT NULL,
  `Apellidos` varchar(150) NOT NULL,
  `Cedula` int(11) NOT NULL,
  `Correo` varchar(150) NOT NULL,
  `FechaNacimiento` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `api_personas`
--

INSERT INTO `api_personas` (`idPersona`, `Nombres`, `Apellidos`, `Cedula`, `Correo`, `FechaNacimiento`) VALUES
(1, 'carlos', 'echenique', 28312806, '', '2001-09-17'),
(3, 'Juan', 'echenique', 28312806, '', '2001-09-17'),
(7, 'Marta', 'Palacios', 4577565, 'carlos@gmail.com', '2014-08-17'),
(8, 'Carlos', 'Palacios', 4577565, 'dasda@gmail.com', '1999-08-17'),
(9, 'Mario', 'Ruiz', 4565758, 'carlos@gmail.com', '1975-02-15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `api_rol`
--

CREATE TABLE `api_rol` (
  `idRol` int(11) NOT NULL,
  `Rol` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `api_rol`
--

INSERT INTO `api_rol` (`idRol`, `Rol`) VALUES
(1, 'Administrador'),
(2, 'Cliente'),
(3, 'Empleado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `api_rolhaspersonas`
--

CREATE TABLE `api_rolhaspersonas` (
  `Rol_idRol` int(11) NOT NULL,
  `Personas_idPersonas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `api_telefonos`
--

CREATE TABLE `api_telefonos` (
  `idTelefono` int(11) NOT NULL,
  `Telefono` int(11) NOT NULL,
  `Codigo de Area_idCodigo de Area` int(11) NOT NULL,
  `Personas_idPersonas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `api_userkeys`
--

CREATE TABLE `api_userkeys` (
  `iduser_keys` int(11) NOT NULL,
  `user_keys` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `api_usuarios`
--

CREATE TABLE `api_usuarios` (
  `idUsuario` int(11) NOT NULL,
  `user` varchar(45) NOT NULL,
  `Password` varchar(45) NOT NULL,
  `Personas_idPersonas` int(11) NOT NULL,
  `user_keys_iduser_keys` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` bigint(20) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add user', 4, 'add_user'),
(14, 'Can change user', 4, 'change_user'),
(15, 'Can delete user', 4, 'delete_user'),
(16, 'Can view user', 4, 'view_user'),
(17, 'Can add content type', 5, 'add_contenttype'),
(18, 'Can change content type', 5, 'change_contenttype'),
(19, 'Can delete content type', 5, 'delete_contenttype'),
(20, 'Can view content type', 5, 'view_contenttype'),
(21, 'Can add session', 6, 'add_session'),
(22, 'Can change session', 6, 'change_session'),
(23, 'Can delete session', 6, 'delete_session'),
(24, 'Can view session', 6, 'view_session'),
(25, 'Can add ciudades', 7, 'add_ciudades'),
(26, 'Can change ciudades', 7, 'change_ciudades'),
(27, 'Can delete ciudades', 7, 'delete_ciudades'),
(28, 'Can view ciudades', 7, 'view_ciudades'),
(29, 'Can add codigo de area', 8, 'add_codigodearea'),
(30, 'Can change codigo de area', 8, 'change_codigodearea'),
(31, 'Can delete codigo de area', 8, 'delete_codigodearea'),
(32, 'Can view codigo de area', 8, 'view_codigodearea'),
(33, 'Can add codigos postales', 9, 'add_codigospostales'),
(34, 'Can change codigos postales', 9, 'change_codigospostales'),
(35, 'Can delete codigos postales', 9, 'delete_codigospostales'),
(36, 'Can view codigos postales', 9, 'view_codigospostales'),
(37, 'Can add estados', 10, 'add_estados'),
(38, 'Can change estados', 10, 'change_estados'),
(39, 'Can delete estados', 10, 'delete_estados'),
(40, 'Can view estados', 10, 'view_estados'),
(41, 'Can add municipios', 11, 'add_municipios'),
(42, 'Can change municipios', 11, 'change_municipios'),
(43, 'Can delete municipios', 11, 'delete_municipios'),
(44, 'Can view municipios', 11, 'view_municipios'),
(45, 'Can add personas', 12, 'add_personas'),
(46, 'Can change personas', 12, 'change_personas'),
(47, 'Can delete personas', 12, 'delete_personas'),
(48, 'Can view personas', 12, 'view_personas'),
(49, 'Can add rol', 13, 'add_rol'),
(50, 'Can change rol', 13, 'change_rol'),
(51, 'Can delete rol', 13, 'delete_rol'),
(52, 'Can view rol', 13, 'view_rol'),
(53, 'Can add telefonos', 14, 'add_telefonos'),
(54, 'Can change telefonos', 14, 'change_telefonos'),
(55, 'Can delete telefonos', 14, 'delete_telefonos'),
(56, 'Can view telefonos', 14, 'view_telefonos'),
(57, 'Can add user keys', 15, 'add_userkeys'),
(58, 'Can change user keys', 15, 'change_userkeys'),
(59, 'Can delete user keys', 15, 'delete_userkeys'),
(60, 'Can view user keys', 15, 'view_userkeys'),
(61, 'Can add direcciones', 16, 'add_direcciones'),
(62, 'Can change direcciones', 16, 'change_direcciones'),
(63, 'Can delete direcciones', 16, 'delete_direcciones'),
(64, 'Can view direcciones', 16, 'view_direcciones'),
(65, 'Can add rol has personas', 17, 'add_rolhaspersonas'),
(66, 'Can change rol has personas', 17, 'change_rolhaspersonas'),
(67, 'Can delete rol has personas', 17, 'delete_rolhaspersonas'),
(68, 'Can view rol has personas', 17, 'view_rolhaspersonas'),
(69, 'Can add usuarios', 18, 'add_usuarios'),
(70, 'Can change usuarios', 18, 'change_usuarios'),
(71, 'Can delete usuarios', 18, 'delete_usuarios'),
(72, 'Can view usuarios', 18, 'view_usuarios');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$390000$W9yHWzlQIPk7V91BrqvvSG$kELOvc98YjIF0gVH6TDOD5uRZn5NP8yjSBVE9/sFc5Y=', '2022-12-07 02:03:51.151058', 1, 'caritoshi', '', '', '', 1, 1, '2022-12-07 01:49:13.378882');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL CHECK (`action_flag` >= 0),
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(7, 'api', 'ciudades'),
(8, 'api', 'codigodearea'),
(9, 'api', 'codigospostales'),
(16, 'api', 'direcciones'),
(10, 'api', 'estados'),
(11, 'api', 'municipios'),
(12, 'api', 'personas'),
(13, 'api', 'rol'),
(17, 'api', 'rolhaspersonas'),
(14, 'api', 'telefonos'),
(15, 'api', 'userkeys'),
(18, 'api', 'usuarios'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(5, 'contenttypes', 'contenttype'),
(6, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` bigint(20) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2022-12-07 01:48:44.885135'),
(2, 'auth', '0001_initial', '2022-12-07 01:48:48.454276'),
(3, 'admin', '0001_initial', '2022-12-07 01:48:48.977354'),
(4, 'admin', '0002_logentry_remove_auto_add', '2022-12-07 01:48:49.141353'),
(5, 'admin', '0003_logentry_add_action_flag_choices', '2022-12-07 01:48:49.290462'),
(6, 'contenttypes', '0002_remove_content_type_name', '2022-12-07 01:48:49.762000'),
(7, 'auth', '0002_alter_permission_name_max_length', '2022-12-07 01:48:50.041999'),
(8, 'auth', '0003_alter_user_email_max_length', '2022-12-07 01:48:50.365000'),
(9, 'auth', '0004_alter_user_username_opts', '2022-12-07 01:48:50.487096'),
(10, 'auth', '0005_alter_user_last_login_null', '2022-12-07 01:48:50.907323'),
(11, 'auth', '0006_require_contenttypes_0002', '2022-12-07 01:48:50.915323'),
(12, 'auth', '0007_alter_validators_add_error_messages', '2022-12-07 01:48:51.024402'),
(13, 'auth', '0008_alter_user_username_max_length', '2022-12-07 01:48:51.249399'),
(14, 'auth', '0009_alter_user_last_name_max_length', '2022-12-07 01:48:51.409049'),
(15, 'auth', '0010_alter_group_name_max_length', '2022-12-07 01:48:51.759111'),
(16, 'auth', '0011_update_proxy_permissions', '2022-12-07 01:48:51.851113'),
(17, 'auth', '0012_alter_user_first_name_max_length', '2022-12-07 01:48:52.030112'),
(18, 'sessions', '0001_initial', '2022-12-07 01:48:52.355114'),
(19, 'api', '0001_initial', '2022-12-07 01:59:07.443125');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('irpwfe8pwz7cbo58niabyq91v6n4gjag', '.eJxVjMsOwiAQRf-FtSHQB3RcuvcbyAwzSNVAUtqV8d-1SRe6veec-1IBtzWHrckSZlZnZdXpdyOMDyk74DuWW9WxlnWZSe-KPmjT18ryvBzu30HGlr91P5kE4BgIvGfsaEgOwDtCNCDRgkmmG1EijBiNJGM9DpPvXU_IVkC9P-Q3OBQ:1p2jmp:ajzS9FS8txJWKkWM6Gh5SE0JcQe3hu_fNdfb4DRJBQs', '2022-12-21 02:03:51.157057');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `api_ciudades`
--
ALTER TABLE `api_ciudades`
  ADD PRIMARY KEY (`idCiudad`);

--
-- Indices de la tabla `api_codigodearea`
--
ALTER TABLE `api_codigodearea`
  ADD PRIMARY KEY (`idCodigo de Area`);

--
-- Indices de la tabla `api_codigospostales`
--
ALTER TABLE `api_codigospostales`
  ADD PRIMARY KEY (`idCodigo Postal`);

--
-- Indices de la tabla `api_direcciones`
--
ALTER TABLE `api_direcciones`
  ADD PRIMARY KEY (`idDirecciones`),
  ADD KEY `api_direcciones_Ciudades_idCiudad_6e3b9c04_fk_api_ciuda` (`Ciudades_idCiudad`),
  ADD KEY `api_direcciones_Codigo Postal_idCodi_95ac0340_fk_api_codig` (`Codigo Postal_idCodigo Postal`),
  ADD KEY `api_direcciones_Estados_idEstado_841a601b_fk_api_estad` (`Estados_idEstado`),
  ADD KEY `api_direcciones_Municipios_idMunicip_18a010eb_fk_api_munic` (`Municipios_idMunicipio`),
  ADD KEY `api_direcciones_Personas_idPersona_a92e3b6c_fk_api_perso` (`Personas_idPersona`);

--
-- Indices de la tabla `api_estados`
--
ALTER TABLE `api_estados`
  ADD PRIMARY KEY (`idEstado`);

--
-- Indices de la tabla `api_municipios`
--
ALTER TABLE `api_municipios`
  ADD PRIMARY KEY (`idMunicipio`);

--
-- Indices de la tabla `api_personas`
--
ALTER TABLE `api_personas`
  ADD PRIMARY KEY (`idPersona`);

--
-- Indices de la tabla `api_rol`
--
ALTER TABLE `api_rol`
  ADD PRIMARY KEY (`idRol`);

--
-- Indices de la tabla `api_rolhaspersonas`
--
ALTER TABLE `api_rolhaspersonas`
  ADD PRIMARY KEY (`Rol_idRol`),
  ADD KEY `api_rolhaspersonas_Personas_idPersonas_fe23d016_fk_api_perso` (`Personas_idPersonas`);

--
-- Indices de la tabla `api_telefonos`
--
ALTER TABLE `api_telefonos`
  ADD PRIMARY KEY (`idTelefono`),
  ADD KEY `api_telefonos_Codigo de Area_idCod_7dda43a0_fk_api_codig` (`Codigo de Area_idCodigo de Area`),
  ADD KEY `api_telefonos_Personas_idPersonas_52d9efb8_fk_api_perso` (`Personas_idPersonas`);

--
-- Indices de la tabla `api_userkeys`
--
ALTER TABLE `api_userkeys`
  ADD PRIMARY KEY (`iduser_keys`);

--
-- Indices de la tabla `api_usuarios`
--
ALTER TABLE `api_usuarios`
  ADD PRIMARY KEY (`idUsuario`),
  ADD KEY `api_usuarios_Personas_idPersonas_dba720c0_fk_api_perso` (`Personas_idPersonas`),
  ADD KEY `api_usuarios_user_keys_iduser_key_22db7815_fk_api_userk` (`user_keys_iduser_keys`);

--
-- Indices de la tabla `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indices de la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indices de la tabla `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indices de la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indices de la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indices de la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indices de la tabla `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indices de la tabla `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `api_ciudades`
--
ALTER TABLE `api_ciudades`
  MODIFY `idCiudad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT de la tabla `api_codigodearea`
--
ALTER TABLE `api_codigodearea`
  MODIFY `idCodigo de Area` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `api_codigospostales`
--
ALTER TABLE `api_codigospostales`
  MODIFY `idCodigo Postal` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `api_direcciones`
--
ALTER TABLE `api_direcciones`
  MODIFY `idDirecciones` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `api_estados`
--
ALTER TABLE `api_estados`
  MODIFY `idEstado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `api_municipios`
--
ALTER TABLE `api_municipios`
  MODIFY `idMunicipio` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `api_personas`
--
ALTER TABLE `api_personas`
  MODIFY `idPersona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `api_rol`
--
ALTER TABLE `api_rol`
  MODIFY `idRol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `api_telefonos`
--
ALTER TABLE `api_telefonos`
  MODIFY `idTelefono` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `api_userkeys`
--
ALTER TABLE `api_userkeys`
  MODIFY `iduser_keys` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `api_usuarios`
--
ALTER TABLE `api_usuarios`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT de la tabla `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `api_direcciones`
--
ALTER TABLE `api_direcciones`
  ADD CONSTRAINT `api_direcciones_Ciudades_idCiudad_6e3b9c04_fk_api_ciuda` FOREIGN KEY (`Ciudades_idCiudad`) REFERENCES `api_ciudades` (`idCiudad`),
  ADD CONSTRAINT `api_direcciones_Codigo Postal_idCodi_95ac0340_fk_api_codig` FOREIGN KEY (`Codigo Postal_idCodigo Postal`) REFERENCES `api_codigospostales` (`idCodigo Postal`),
  ADD CONSTRAINT `api_direcciones_Estados_idEstado_841a601b_fk_api_estad` FOREIGN KEY (`Estados_idEstado`) REFERENCES `api_estados` (`idEstado`),
  ADD CONSTRAINT `api_direcciones_Municipios_idMunicip_18a010eb_fk_api_munic` FOREIGN KEY (`Municipios_idMunicipio`) REFERENCES `api_municipios` (`idMunicipio`),
  ADD CONSTRAINT `api_direcciones_Personas_idPersona_a92e3b6c_fk_api_perso` FOREIGN KEY (`Personas_idPersona`) REFERENCES `api_personas` (`idPersona`);

--
-- Filtros para la tabla `api_rolhaspersonas`
--
ALTER TABLE `api_rolhaspersonas`
  ADD CONSTRAINT `api_rolhaspersonas_Personas_idPersonas_fe23d016_fk_api_perso` FOREIGN KEY (`Personas_idPersonas`) REFERENCES `api_personas` (`idPersona`),
  ADD CONSTRAINT `api_rolhaspersonas_Rol_idRol_7ee97cbb_fk_api_rol_idRol` FOREIGN KEY (`Rol_idRol`) REFERENCES `api_rol` (`idRol`);

--
-- Filtros para la tabla `api_telefonos`
--
ALTER TABLE `api_telefonos`
  ADD CONSTRAINT `api_telefonos_Codigo de Area_idCod_7dda43a0_fk_api_codig` FOREIGN KEY (`Codigo de Area_idCodigo de Area`) REFERENCES `api_codigodearea` (`idCodigo de Area`),
  ADD CONSTRAINT `api_telefonos_Personas_idPersonas_52d9efb8_fk_api_perso` FOREIGN KEY (`Personas_idPersonas`) REFERENCES `api_personas` (`idPersona`);

--
-- Filtros para la tabla `api_usuarios`
--
ALTER TABLE `api_usuarios`
  ADD CONSTRAINT `api_usuarios_Personas_idPersonas_dba720c0_fk_api_perso` FOREIGN KEY (`Personas_idPersonas`) REFERENCES `api_personas` (`idPersona`),
  ADD CONSTRAINT `api_usuarios_user_keys_iduser_key_22db7815_fk_api_userk` FOREIGN KEY (`user_keys_iduser_keys`) REFERENCES `api_userkeys` (`iduser_keys`);

--
-- Filtros para la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Filtros para la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Filtros para la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
